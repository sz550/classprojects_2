"""print the results in output.log and the errors in output_err.log"""
import sys
class Logger(object):
    def __init__(self, filename='default.log', stream=sys.stdout):
        self.terminal = stream
        self.log = open(filename, 'w')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass

sys.stdout = Logger('output.log', sys.stdout)
sys.stderr = Logger('output_err.log', sys.stderr)

"""chemical potential equalization.

Usage:
Prepare two files. One is the PDB file, such as some.pdb,
and another one stores the data of electronegativity and hardness.
The format of the data file is:
1,  H,   7.18,   13.59
where 7.18 is electronegativity and 13.59 is hardness.

Then call the calculateCoefficients function:
>>> from cpe import calculateCoefficients
>>> calculateCoefficients("some.pdb")

The calculateCoefficients function will returns an NumPy array
containing coefficients.
"""
import numpy as np
import scipy
import scipy.spatial
import scipy.special
from typing import Tuple, List

# unit constant
#Hartree = 27.211386024367243
HartreeIneV = 27.211386024367243
BohrInAngstrom = 0.529177249

def readData(fn: str) -> Tuple[dict, dict]:
    """Read electronegativity and hardness from data file.

    Input: fn: filename of data
    Return: two dicts that contains electronegativity and hardness
    """
    chi = {}
    eta = {}
    with open(fn) as f:
        for line in f:
            if line.startswith("#"):
                # skip comment line
                continue
            s = line.split(",")
            # number element chi eta
            if len(s) == 4:
                name = s[1].strip()
                chi[name] = float(s[2])
                eta[name] = float(s[3])
    return chi, eta


class Atom:
    """Atom (data structure)
    Crd[:]   - Cartesian coordinate x,y,z (A)
    radius   - solvation radius (A)
    charge   - permanent charge (au)
    chi      - electronegativity (eV)
    eta      - hardness (eV)
    gaussExp - Gaussian exponent (dtermined from eta) (au)
    mu0      - chemical potential (determined from electronegativity) (au)
    """

    def __init__(self, crd: np.ndarray, radius: float, charge: float, chi: float, eta: float):
        """Input: crd, radius, charge, chi, eta
        gaussExp and mu0 will be calculated
        """
        self.crd = crd / BohrInAngstrom
        self.radius = radius / BohrInAngstrom
        self.charge = charge
        self.chi = chi / HartreeIneV
        self.eta = eta / HartreeIneV
        # Gaussian exponent = sqrt(2*pi) * hardness
        self.gaussExp = np.sqrt(2*np.pi) * (0.5 * self.eta )
        # the chemical potential is the negative of electronegativity
        self.mu0 = self.chi


class Atoms:
    """Contains a list of atoms"""

    def __init__(self, atoms: List[Atom] = None):
        if atoms:
            self.atoms = atoms
        else:
            self.atoms = []

    def append(self, atom: Atom):
        """Append a atom."""
        self.atoms.append(atom)

    def __getitem__(self, idx: int) -> Atom:
        """Returns: atom by idx"""
        return self.atoms[idx]

    @property
    def crd(self) -> np.ndarray:
        """Returns: Cartesian coordinates of atoms, shape: N*3"""
        return np.stack(tuple((atom.crd for atom in self.atoms)))

    @property
    def gaussExp(self) -> np.ndarray:
        """Returns: gaussExp of atoms, shape: N"""
        return np.array(tuple((atom.gaussExp for atom in self.atoms)))

    @property
    def mu0(self) -> np.ndarray:
        """Returns: mu0 of atoms, shape: N"""
        return np.array(tuple((atom.mu0 for atom in self.atoms)))

    @property
    def charge(self) -> np.ndarray:
        """Returns: charge of atoms, shape: N"""
        return np.array(tuple((atom.charge for atom in self.atoms)))


def readInput(filename: str, datafile: str = "data.dat") -> Atoms:
    """read in atomic information from file

    The format of PDB files is:
    ATOM    960  C   TRP A 123       0.791  24.231  12.854  1.00 14.29           C
    ATOMID, ATOMNUM, ATOMNAME, RESNAME, CHAINID, RESNUM, X, Y, Z, 
    BVALUE=SOLVATION RADIUS, OCC=PARTIAL CHARGE, ELEMENT SYMBOL

    Input: filename
    Output: nAtom, Atom
    """
    # read data
    chi, eta = readData(datafile)
    atoms = Atoms()
    with open(filename) as f:
        for line in f:
            if line.startswith("ATOM") or line.startswith("HETATM"):
                s = line.split()
                element = s[2][0]
                if len(s[2])>1 and s[2][1].islower():
                    element += s[2][1]
                atoms.append(Atom(
                    crd=np.array(tuple(map(float, s[-5:-2]))),
                    radius=float(s[-2]),
                    charge=float(s[-1]),
                    chi=chi[element],
                    eta=eta[element],
                ))
    return atoms


def electrostaticInteractionMatrix(atoms: Atoms) -> np.ndarray:
    """The matrix will be
    erf[alpha1*alpha2/(alpha1^2+alpha2^2)^(1/2) R12]/R12, if i != j
    alpha*sqrt(2/pi), if i = j

    Input: atoms
    Returns: (nAtom x nAtom) Gaussian-Gaussian electrostatic interaction
            matrix between atom-centered L1-normalized Gaussian functions
            with Gaussian exponents gaussExp, and centered on atomic
            positions.
    """
    gaussExp = atoms.gaussExp
    # diagonal: alpha * sqrt(2/pi)
    diag = gaussExp * np.sqrt(2/np.pi)
    # alpha1 * alpha2, shape: N*N
    mul = np.outer(gaussExp, gaussExp)
    # alpha1^2+alpha2^2, shape: N*N
    squ = np.square(gaussExp)
    squ_add = np.add.outer(squ, squ)
    # the distance matrix, implemented by a scipy function
    # shape: N*N
    dm = scipy.spatial.distance_matrix(atoms.crd, atoms.crd)
    # erf[mul/sqrt(squ_add) * dm]/dm, shape: N*N
    with np.errstate(divide='ignore', invalid='ignore'):
        m = scipy.special.erf(mul/np.sqrt(squ_add) * dm) / dm
        # fill diagonal
        np.fill_diagonal(m, diag)
    return np.matrix(m)


def calculateCoefficients(fn: str, Qbias:float = 0., datafile: str = "../data/AtomicElectronegativityAndHardness.dat") -> np.ndarray:
    """Calculate coefficients

    Input: fn: PDB filename
    Returns: coefficients"""
    # read in atomic information from file
    atoms = readInput(fn, datafile=datafile)
    etaM = electrostaticInteractionMatrix(atoms)
    mu0V = atoms.mu0.reshape((-1, 1))
    # Let dN = 0.0 to start - but keep it as a variable.
    dN = np.sum(atoms.charge) + Qbias
    # solve for dQ: EtaM * dQ = -mu0V + d*lambda
    # where d[:] = 1 and lambda = Lagrange multiplier on constraint condition dQ*d = dN.
    d = np.ones_like(mu0V)
    # See Eq. 13-14
    la = (dN + np.dot(np.dot(d.T, etaM.I), mu0V)) / \
        (np.dot(np.dot(d.T, etaM.I), d))
    la = float(la)
    dQ = np.dot(etaM.I, la * d - mu0V)
    #dQ = dQ.flatten()
    # E(c) = mu0 c^T . d + c^T . dv + 1/2 * c^T . etaM . c
    # See Eq (10a)
    energy =  la * np.dot(dQ.T, d) + np.dot(dQ.T, mu0V) + 0.5 * np.dot(np.dot(dQ.T, etaM), dQ)
    return atoms, dQ.flatten(), float(energy.flatten())

def calculateDipoleMoment(atoms, dQ):
    """Calculate dipole moment
    \sum_k q_k * (r_k - r_0)
    """
    center = np.mean(atoms.crd, axis=0)
    dipole = np.dot(dQ.reshape(-1), atoms.crd - center)
    return np.linalg.norm(dipole)



if __name__ == "__main__":
    from glob import glob
    print("#", "filename", "E[Q]", "E[Q+1]", "E[Q-1]", "dipoleMoment")
    for pdbfn in glob("../molecules/*.pdb"):
        #atoms, dQ, energy = calculateCoefficients(pdbfn, 0., datafile="../data/AtomicElectronegativityAndHardness.dat")
        # format output
        #print(pdbfn)
        #print()
        atoms, dQ, e1 = calculateCoefficients(pdbfn, 0.)
        _, _, e2 = calculateCoefficients(pdbfn, 1.)
        _, _, e3 = calculateCoefficients(pdbfn, -1.)
        dipole = calculateDipoleMoment(atoms, dQ)
        dQ = np.around(dQ, 4)
        print("%s %.4f %.4f %.4f %.4f %.4f %.4f"%(pdbfn, e1*HartreeIneV, e2*HartreeIneV, e3*HartreeIneV, ((e2+e3)/2)*HartreeIneV, ((e2-e3)/2)*HartreeIneV, dipole))
        #print("# dQ ref")
        for ii in range(dQ.size):
            print(ii, dQ[0][ii], atoms[ii].charge)
        print()
